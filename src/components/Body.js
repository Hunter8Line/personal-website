import React from 'react';
import {Route} from "react-router-dom";
import {Switch} from "react-router";
import {projects} from "../pages/projects";
import {contact} from "../pages/contact";
import homepage from "../pages/homepage";


const Body = () =>
    <div>
        <Switch>
            <Route exact path={"/projects"} component={projects}/>
            <Route path={"/contact"} component={contact}/>
            <Route component={homepage}/>
        </Switch>
    </div>;
export default Body;