import React from "react";
import wide from "../images/logo/wide.png";

export const Footer = () =>
    <footer className="footer footer-dark bg-primary fixed-bottom">
        <div className="container">
            <span className="text-light">
                &copy; 2016-{new Date().getFullYear()}
                <img style={{width: '48px', height: '24px'}} src={wide} alt="Hunter Line" className="pl-2"/>
            </span>
        </div>
    </footer>;
export default Footer;