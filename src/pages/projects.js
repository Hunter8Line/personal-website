import React from "react";
import {
    Alert,
    Card,
    CardBody,
    CardDeck,
    CardImg,
    CardImgOverlay,
    CardSubtitle,
    CardText,
    CardTitle,
    Container
} from "reactstrap";
import code from "../images/code.png";
import heyya from "../images/heyya.png"

export const projects = () =>
    <div>
        <Card inverse className="border-0 pb-2">
            <CardImg
                src={code}
                className="img-fluid rounded-bottom banner"
                alt=""/>
            <CardImgOverlay className="alignmiddle">
                <div className="text-center marker banner code my-auto">
                    <CardTitle className="h2 display-1 my-auto">Projects</CardTitle>
                </div>
            </CardImgOverlay>
        </Card>
        <Container>
            <Alert color="warning text-center">
                This page is still being worked on. Sorry!
            </Alert>
            <CardDeck>
                <Card>
                    <CardImg top width="100%"
                             src="https://via.placeholder.com/354x215?text=hunter.tline.dev"
                             alt="Card image cap"/>
                    <CardBody>
                        <CardTitle>This Site</CardTitle>
                        <CardSubtitle>hunter.tline.dev</CardSubtitle>
                        <CardText>This is a wider card with supporting text below as a natural lead-in to additional
                            content. This content is a little bit longer.</CardText>
                        <a href="https://hunter.tline.dev" className="btn btn-secondary">View Site</a>
                    </CardBody>
                </Card>
                <Card>
                    <CardImg top width="100%"
                             src="https://via.placeholder.com/354x215?text=NLSoftworks"
                             alt="Card image cap"/>
                    <CardBody>
                        <CardTitle>NLSoftworks</CardTitle>
                        <CardSubtitle>A Software Engineering Company</CardSubtitle>
                        <CardText>Co-Found and system administrator for the development server.</CardText>
                        <a href="https://nlsoftworks.com" className="btn btn-secondary">View Site</a>
                    </CardBody>
                </Card>
                <Card>
                    <CardImg top width="100%"
                             src={heyya}
                             alt="Card image cap"/>
                    <CardBody>
                        <CardTitle>Hey Ya!</CardTitle>
                        <CardSubtitle>The messenger for you.</CardSubtitle>
                        <CardText>Co-Creator of a basic and simple messaging platform built with React and Google
                            Firebase.</CardText>
                        <a href="https://heyya.app" className="btn btn-secondary">View Site</a>
                    </CardBody>
                </Card>
            </CardDeck>
        </Container>
    </div>;