import React from "react";
import {
    Alert,
    Button,
    Card,
    CardBody,
    CardImg,
    CardImgOverlay,
    CardTitle,
    Container,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import code from "../images/code.png";

export const contact = () =>
    <div>
        <Card inverse className="border-0 pb-2">
            <CardImg
                src={code}
                className="img-fluid rounded-bottom banner"
                alt=""/>
            <CardImgOverlay className="alignmiddle">
                <div className="text-center marker banner code my-auto">
                    <CardTitle className="h2 display-1 my-auto">Contact Me</CardTitle>
                </div>
            </CardImgOverlay>
        </Card>
        <Container>
            <Alert color="warning text-center">
                This page is still being worked on. Sorry!
            </Alert>
            <Card className="py-2">
                <CardBody>
                    <Form action="https://formspree.io/hunter@tline.dev" method="POST">
                        <FormGroup>
                            <Label for="Name">Name</Label>
                            <Input type="text" name="name" id="Name" placeholder="John Smith"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="Email">Email</Label>
                            <Input type="email" name="_replyto" id="Email" placeholder="example@gmail.com"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="Message">Text Area</Label>
                            <Input type="textarea" name="message" id="Message"/>
                        </FormGroup>
                        <Button type="submit" value="send">Submit</Button>
                    </Form>
                </CardBody>
            </Card>
        </Container>
    </div>;