import React from "react";
import profile from "../images/profile.jpg"
import code from "../images/code.png";
import {Button, Card, CardImg, CardImgOverlay, CardText, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";

const homepage = () =>
    <div>
        <Card inverse className="border-0">
            <CardImg
                src={code}
                className="img-fluid rounded-bottom banner-home"
                alt=""/>
            <CardImgOverlay>
                <div className="text-center marker banner code">
                    <CardTitle className="h1 display-1">Hunter Line</CardTitle>
                    <CardText className="h3 display-3">Software Developer</CardText>
                </div>
            </CardImgOverlay>
        </Card>
        <div className="container pt-1">
            <div className="row home">
                <div className="col-md-4 d-flex justify-content-center"><img className="rounded-circle" src={profile}
                                                                             alt="Hunter Line"/></div>
                <div className="col-md-8">
                    <h1>About Me</h1>
                    <p>
                        I am from Fort Wayne, Indiana. Currently residing in Muncie, Indiana as I attend Ball State
                        University.
                        I am currently seeking a Bachelor's of Science in Computer Science with a minors in
                        Computer
                        Technology and Sociology.
                        I grew up with a family computer which I experimented with and looked to try new and
                        different
                        things with it and continue this trend of experimenting with computers and technology to
                        this day.
                    </p>
                    <Link to={"/projects"}><Button size="lg" color="primary">My Projects</Button></Link>

                </div>
            </div>
        </div>

    </div>
;
export default homepage;