import React, {Component} from 'react';
import Topbar from "./components/Topbar";
import Footer from "./components/Footer";
import Body from "./components/Body";
import {BrowserRouter as Router} from "react-router-dom";

class App extends Component {
    render() {
        return (
            <div>
                <Topbar/>
                <Router history={this.props}>
                    <Body/>
                </Router>
                <Footer/>
            </div>
        );
    }
}

export default App;
